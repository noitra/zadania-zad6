__author__ = 'Noitra'
from django.contrib import admin
from django import forms
from models import Tweet


class TweetModelForm( forms.ModelForm ):
    content = forms.CharField(widget=forms.Textarea)
    class Meta:
        model = Tweet


class TweetAdmin(admin.ModelAdmin):
    form = TweetModelForm
    list_display = ('title', 'pub_date')
    list_filter = ['pub_date']
    search_fields = ['title']
    date_hierarchy = 'pub_date'


admin.site.register(Tweet, TweetAdmin)