# Create your views here.
from django.shortcuts import render, get_object_or_404
from models import Tweet


def index(request):
    latest_tweet_list = Tweet.objects.all().order_by('-pub_date')
    context = {'latest_tweet_list': latest_tweet_list}
    return render(request, 'mblog/index.html', context)


def detail(request, tweet_id):
    tweet = get_object_or_404(Tweet, pk=tweet_id)
    return render(request, 'mblog/detail.html', {'tweet': tweet})