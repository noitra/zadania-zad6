__author__ = 'Noitra'
from django.conf.urls import patterns, url

import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    # ex: /full/5/
    url(r'full/(?P<tweet_id>\d+)/$', views.detail, name='detail'),
)